#include "command_parser.h"
#include "uart_driver.h"
#include "main.h"
#include <string.h>



extern uart_driver_t uart_driver;

const uint8_t read_temperature_cmd[] = "READ_TEMPERATURE";
const uint8_t read_fan_speed_cmd[] = "READ_FAN_SPEED";
const uint8_t set_fan_speed_cmd[] = "SET_FAN_SPEED";
const uint8_t close_door_cmd[] = "CLOSE_DOOR";
const uint8_t open_door_cmd[] = "OPEN_DOOR";
const uint8_t get_door_position_cmd[] = "GET_DOOR_POSITION";
const uint8_t get_fw_version_cmd[] = "GET_FW_VERSION";
const uint8_t get_unit_tick_cmd[] = "GET_UNIT_TICK";
const uint8_t get_heater_state_cmd[] = "GET_HEATER_STATE";

const uint8_t ack_message[] = "*ACK#";
const uint8_t ack_message1[] = "*ACK_Rfs#";
const uint8_t ack_message2[] = "*ACK_Sfs#";
const uint8_t ack_message3[] = "*ACK_CD#";
const uint8_t ack_message4[] = "*ACK_OD#";
const uint8_t ack_message5[] = "*ACK_GDP#";
const uint8_t ack_message6[] = "*ACK_GFV#";
const uint8_t ack_message7[] = "*ACK_GUT#";
const uint8_t ack_message8[] = "*ACK_GHS#";


const uint8_t nack_message[] = "NACK";

void parse_command(uint8_t *rx_packet )
{
	if(memcmp(rx_packet, read_temperature_cmd, sizeof (read_temperature_cmd)-1)==0){
	/// LO QUE ME DEBE HACER

		uart_driver_send(&uart_driver,(uint8_t *)ack_message, sizeof(ack_message)-1);

	}else if (memcmp(rx_packet, read_fan_speed_cmd, sizeof (read_fan_speed_cmd)-1)==0){
		uart_driver_send(&uart_driver,(uint8_t *)ack_message1, sizeof(ack_message1)-1);
	}else if (memcmp(rx_packet, set_fan_speed_cmd, sizeof (set_fan_speed_cmd)-1)==0){
		uart_driver_send(&uart_driver,(uint8_t *)ack_message2, sizeof(ack_message2)-1);
	}else if (memcmp(rx_packet, close_door_cmd, sizeof (close_door_cmd)-1)==0){
		uart_driver_send(&uart_driver,(uint8_t *)ack_message3, sizeof(ack_message3)-1);
	}else if (memcmp(rx_packet, open_door_cmd, sizeof (open_door_cmd)-1)==0){
		uart_driver_send(&uart_driver,(uint8_t *)ack_message4, sizeof(ack_message4)-1);
	}else if (memcmp(rx_packet, get_door_position_cmd, sizeof (get_door_position_cmd)-1)==0){
		uart_driver_send(&uart_driver,(uint8_t *)ack_message5, sizeof(ack_message5)-1);
	}else if (memcmp(rx_packet, get_fw_version_cmd, sizeof (get_fw_version_cmd)-1)==0){
		uart_driver_send(&uart_driver,(uint8_t *)ack_message6, sizeof(ack_message6)-1);
	}else if (memcmp(rx_packet, get_unit_tick_cmd, sizeof (get_unit_tick_cmd)-1)==0){
		uart_driver_send(&uart_driver,(uint8_t *)ack_message7, sizeof(ack_message7)-1);
	}else if (memcmp(rx_packet, get_heater_state_cmd, sizeof (get_heater_state_cmd)-1)==0){
		uart_driver_send(&uart_driver,(uint8_t *)ack_message8, sizeof(ack_message8)-1);
	}else {
		uart_driver_send(&uart_driver, (uint8_t *)nack_message, sizeof(nack_message)-1);
	}
}
